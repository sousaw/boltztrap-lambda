#!/usr/bin/perl -w
use strict;

my (@temp);
my ($pip,$pip2,$i,$j,$name,$nat,$enetest);

die "Usage: gather_energy.pl [case] \n" if $#ARGV < 0;
$name = $ARGV[0];

open(STRUC,"$name.struct");
$nat = 0;
while(<STRUC>) {
    $nat++ if (/RMT=/);
}
close(STRUC);
if( -s "$name.inso" ){
    system("cat $name.energyso_? $name.energyso_?? > pip") ;
    open(ENE,">$name.energyso");
} else {
    system("cat $name.energy_? $name.energy_?? > pip") ;
    open(ENE,">$name.energy");
}

open(PIPF,"pip");
$enetest = <PIPF>;
printf ENE "$enetest";
for ($j = 2; $j <= (2*$nat); $j++) {
    $pip = <PIPF>;
    printf ENE "$pip";
}
while (<PIPF>) {
    if(/$enetest/) {
	for ($j = 2; $j <= (2*$nat); $j++) {
	    $pip = <PIPF>;
	}
	
    } else {
	printf ENE $_;
    }
}
close(ENE);
close(PIPF);
