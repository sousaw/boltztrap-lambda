MODULE lattice_points
  INTEGER                   :: nwave
  REAL(8), POINTER          :: latt_points(:,:)

CONTAINS
  SUBROUTINE gen_lattpoints(aac,nsym,symop,gmax,idebug)
    ! Georg Madsen University of Aarhus 2006
    ! Fills all lattice vectors with length shorther than gmax into the 
    ! array latt_points
    ! aac (the direction cosines) must refer to the primitive lattice
    ! SORTAG stolen from WIEN code
    USE defs
    USE reallocate
    IMPLICIT NONE
    INTEGER,INTENT(IN)       :: nsym,idebug
    REAL(8),INTENT(IN)       :: aac(3,3),gmax,symop(3,3,48)
    REAL(8)                  :: aacr(3,3),metten(1:3,1:3)
    INTEGER                  :: kmax1(3)
    INTEGER                  :: m(3),nst
    REAL(8)                  :: stg(3,nsym),m2(3),kdum(3)
    LOGICAL                  :: KDELTA,addvec
    !                                                                       
    REAL(8), POINTER         :: absk(:)
    REAL(8)                  :: absm
    INTEGER                  :: i,j,i1,i2,i3
    INTEGER                  :: nwav1,ind,nradm,irad,istart,islut
    REAL(8),ALLOCATABLE      :: radii(:)
    INTEGER,ALLOCATABLE      :: icnt(:),ihkl1(:,:),ihkl2(:,:)
    INTEGER :: clock_start,clock_end,clock_rate
    REAL(8) :: elapsed_time

    WRITE(6,*) '======= OUTPUT FROM gen_lattpoints ======================'
    CALL SYSTEM_CLOCK(COUNT_RATE=clock_rate) ! Find the rate
    CALL SYSTEM_CLOCK(COUNT=clock_start) ! Start timing

    metten=MATMUL(TRANSPOSE(aac),aac)
    CALL invert_3x3(aac,aacr,i)
    aacr=TRANSPOSE(aacr)

    DO i=1,3
       kmax1(i)=gmax*SQRT(DOT_PRODUCT(aacr(1:3,i),aacr(1:3,i)))+1
    ENDDO
    WRITE(6,*) ' KXMAX,KYMAX,KZMAX',kmax1(1:3)
    WRITE(6,*) 'GMAX',gmax
    nradm=PRODUCT(2*kmax1+1)
    ALLOCATE(icnt(nradm),radii(nradm),ihkl1(3,nradm))
    irad=0
    DO I3=-kmax1(3),kmax1(3)
       M(3)=I3
       DO I2=-kmax1(2),kmax1(2)
          M(2)=I2
          DO I1=-kmax1(1),kmax1(1)
             M(1)=I1
             m2=m
             absm=DOT_PRODUCT(m2(1:3),MATMUL(m2(1:3),metten(1:3,1:3)))
             if(absm.GT.TEST) absm=SQRT(absm)
             if(absm.gt.gmax) CYCLE
             irad=irad+1
             icnt(irad)=irad
             radii(irad)=absm
             ihkl1(1:3,irad)=m(1:3)
          ENDDO
       ENDDO
    ENDDO
!     Quicksort over radii
    CALL sortag(radii,irad,icnt)
!     Now re-order equal radii
!     Copy a sorted hkl list from ihkl1 to ihkl2
    allocate (ihkl2(3,irad))
    do j=1,irad
       i=icnt(j)
       ihkl2(1:3,j)=ihkl1(1:3,i)
    enddo
    deallocate (ihkl1)

    NWAV1=11111  
    allocate ( latt_points(3,nwav1), absk(nwav1) )
    absk=0.0d0
    NWAVE=1
    m2(1:3)=ihkl2(1:3,1)
    latt_points(1:3,nwave)=ihkl2(1:3,1)
    absk(nwave)=radii(1)
    DO j=2,irad      
       m2(1:3)=ihkl2(1:3,j)
!       absm=SQRT(DOT_PRODUCT(m2(1:3),MATMUL(m2(1:3),metten(1:3,1:3))))
       absm=radii(j)
       addvec=.FALSE.
       ! NEW vector of diffent length
       IF(ABSM-ABSK(nwave)>0.0001) THEN
          istart=nwave+1
          addvec=.true.
       ENDIF
       ! .... NEW VECTOR EQU. OLD ONE
       IF(.NOT.addvec) THEN
          CALL stern1(m2,nsym,symop,nst,stg)
          DO I=istart,islut
             kdum(1:3)=latt_points(1:3,I)
             IF(KDELTA(kdum,nst,stg)) GOTO 1
          ENDDO
          addvec=.TRUE.
1         CONTINUE
       ENDIF
       ! .... PUT NEW VECTOR IN LIST
       IF(addvec) THEN
          nwave=nwave+1
          IF(nwave.GE.NWAV1) then
             nwav1=nwav1*2
             CALL doreallocate(latt_points, 3, nwav1)
             CALL doreallocate(absk, nwav1)
          ENDIF
          latt_points(1:3,nwave)=m2
          absk(nwave)=absm
          islut=nwave
       ENDIF
    ENDDO
    
    WRITE(6,1010) nwave

    call doreallocate(latt_points, 3, nwave)
    IND=0
    DO I=1,nwave
       M2(1:3)=latt_points(1:3,I)
       CALL STERN1(m2,nsym,symop,nst,stg) 
       IF(idebug>0) WRITE(6,1020) I,latt_points(1:3,I),absk(i),nst
       IF(idebug>3) THEN
          WRITE(6,1021) (stg(1:3,j),j=1,nst)
       ENDIF
       ind=ind+nst
    ENDDO
    WRITE(6,1030) IND
    DEALLOCATE (absk)
    DEALLOCATE(icnt,radii,ihkl2)

    CALL SYSTEM_CLOCK(COUNT=clock_end) ! Stop timing
    elapsed_time=REAL(clock_end-clock_start)/clock_rate
    WRITE(6,*) 'USED TIME:',elapsed_time
    WRITE(6,*) '=============== END gen_lattpoints ======================'
    RETURN                                                            

1010 FORMAT(3X,I6,' LATTICE POINTS GENERATED ',/)
1020 FORMAT(7X,'KVEC(',I8,') = ',3f5.0,f10.4,i5)   
1021 FORMAT(24X,3f5.0)   
1030 FORMAT(8X,'SIZE INCLUDING STAR MEMBERS = ',I8)                    
  END SUBROUTINE GEN_LATTPOINTS


END MODULE lattice_points
