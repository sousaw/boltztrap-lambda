SUBROUTINE wien_band
  USE reallocate
  USE defs
  USE input
  USE bandstructure
  IMPLICIT NONE
  INTEGER                  :: nat
! Georg Madsen 2004

  WRITE(6,'(A)') '==============  OUTPUT from WIEN interface ==============='
!  CALL wien_GTFNAM(DEFFN,ERRFN,iproc)

  CALL wien_structfile(nat)

  CALL wien_energyfiles(nat)
  WRITE(6,'(A)') '==============  End WIEN interface         ==============='

END SUBROUTINE wien_band

SUBROUTINE wien_structfile(nat)
  USE reallocate
  USE defs
  USE input
  USE bandstructure
  IMPLICIT NONE

  CHARACTER(80)             :: title
  CHARACTER(4)              :: lattic,irel
  REAL(8)                   :: test0

  INTEGER                   :: nat,ndif,iz(3,3,48)
  INTEGER,ALLOCATABLE       :: mult(:),jrj(:),iatnr(:),isplit(:)
  REAL(8),ALLOCATABLE       :: r0(:),dx(:),rmt(:),zz(:)
  CHARACTER(10),ALLOCATABLE :: aname(:)
  REAL(8),POINTER           :: pos(:,:)

  INTEGER                   :: iord
  INTEGER,POINTER           :: inum(:)
  REAL(8),ALLOCATABLE       :: rotloc(:,:,:)
  INTEGER                   :: i,j,m,j1,j2,index,jatom,ios

    test0=1.D-5
  read (20,1000) title
  read (20,1010) lattic,nat,irel
  ALLOCATE(aname(nat))
  allocate(mult(0:nat))
  allocate(jrj(nat))
  allocate(r0(nat))
  allocate(dx(nat))
  allocate(rmt(nat))
  allocate(zz(nat))
  allocate(rotloc(3,3,nat))
  allocate(iatnr(nat))
  allocate(isplit(nat))
  ALLOCATE (pos(3,48*nat))
  mult(0)=0
  read (20,1020) aa(1:3),alpha(1),alpha(2),alpha(3)
  IF(ABS(ALPHA(1)).LT.test0) ALPHA(1)=ninety
  IF(ABS(ALPHA(2)).LT.test0) ALPHA(2)=ninety
  IF(ABS(ALPHA(3)).LT.test0) ALPHA(3)=ninety
  INDEX=0
  DO jatom=1,NAT
     INDEX=INDEX+1
     READ(20,1030,iostat=ios) iatnr(jatom),( pos(j,index),j=1,3 ), &
          mult(jatom),isplit(jatom) 
     IF(ios /= 0) THEN
        WRITE(6,*) iatnr(jatom),( pos(j,index),j=1,3 ), &
             mult(jatom),isplit(jatom) 
        WRITE(6,*) 'ERROR IN STRUCT FILE READ'
        STOP
     ENDIF
     IF (mult(jatom) .EQ. 0) THEN
        WRITE (6,6000) jatom, index, mult(jatom)
        STOP
     ENDIF
     DO m=1,mult(jatom)-1                                     
        index=index+1                                            
        READ(20,1031) iatnr(jatom),( pos(j,index),j=1,3)         
     ENDDO
     READ(20,1050) aname(jatom),jrj(jatom),r0(jatom),rmt(jatom), &
          zz(jatom)
     dx(jatom)=LOG(rmt(jatom)/r0(jatom)) / (jrj(jatom)-1)           
     rmt(jatom)=r0(jatom)*EXP( dx(jatom)*(jrj(jatom)-1) )           
     READ(20,1051) ((rotloc(i,j,jatom),i=1,3),j=1,3)                
  ENDDO
  ndif=index
  CALL doreallocate(pos, 3, ndif)
  READ(20,1151) iord
  nsym=iord
  ALLOCATE(inum(nsym))
  DO j=1,iord
     READ(20,1101) ( (iz(j1,j2,j),j2=1,3),tau(j1,j),j1=1,3 ),inum(j)
  ENDDO
  deallocate (inum)
  ALPHA(1:3)=ALPHA(1:3)*PI/180.0D0
  SELECT CASE (LATTIC(1:1))
  CASE ('H','P')       !.....Primitive lattices
     lattice='P'
  CASE ( 'F')
     lattice='F'
  CASE ( 'B' )
     lattice='I'
  CASE( 'C' )
     SELECT CASE (lattic(2:3))
     CASE ( 'XY' )
        lattice='C'
     CASE( 'XZ ' )
        lattice='B'
     CASE( 'YZ' )
        lattice='A'
     END SELECT
  CASE ( 'R' )
     lattice='R'
  CASE DEFAULT
     STOP 'LATGEN - Wrong lattice'
  END SELECT
  CALL latgen2('xayb',lattice,aa,alpha, &
       volume,aac_dir,p2c_dir,aac_rec,p2c_rec)

!!$  temp=MATMUL(aac_rec,p2c_rec)
!!$  WRITE(6,106)((temp(i,j),j=1,3),i=1,3)
!!$  STOP

  IF(lattice=='P'.OR.lattice=='R') THEN
     ! Symmetry operators allready primitive
     DO index=1,nsym
        symop(1:3,1:3,index)=iz(1:3,1:3,index)
     ENDDO
  ELSEIF(lattice=='F'.OR.lattice=='I'.OR.lattice=='A'.OR.lattice=='B'.OR.lattice=='C') THEN
     ! Convert symmetry operators to primitive
     DO index=1,nsym
        symop(1:3,1:3,index)=iz(1:3,1:3,index)
        symop(1:3,1:3,index)=MATMUL(TRANSPOSE(p2c_rec),MATMUL(symop(1:3,1:3,index),p2c_dir))
     ENDDO
  ELSE
     STOP 'UNKNOWN LATTICE TYPE'
  ENDIF

  IF(idebug>0) THEN
     WRITE(6,*) 'Symmetry operators in primitive basis'
     DO index=1,nsym
        WRITE(6,*) '---------'
        WRITE(6,106)((symop(i,j,index),j=1,3),i=1,3)
     ENDDO
  ENDIF
106 FORMAT(3(10X,3F10.5,/))                      


  deALLOCATE(aname)
  deallocate(mult)
  deallocate(jrj)
  deallocate(r0)
  deallocate(dx)
  deallocate(rmt)
  deallocate(zz)
  deallocate(rotloc)
  deallocate(iatnr)
  deallocate(isplit)
  deALLOCATE (pos)

1000 FORMAT(A80)                                                       
1010 FORMAT(A4,24X,I2,/,13X,A4)                                 
1020 FORMAT(6F10.7,10X,F10.7)                                          
1030 FORMAT(5X,I3,4X,F10.7,3X,F10.7,3X,F10.7,/,15X,I2,17X,I2)          
1031 FORMAT(5X,I3,4X,F10.7,3X,F10.7,3X,F10.7)                          
1050 FORMAT(A10,5X,I5,5X,F10.9,5X,F10.5,5X,F10.5)                      
1051 FORMAT(20X,3F10.8)                                             
1101 FORMAT(3(3I2,F10.8/),I8)
1151 FORMAT(I4)
6000 FORMAT(///,3X,'ERROR IN READING STRUCT : MULT(JATOM)=0 ...', &
       /, 20X,'JATOM=',I3,3X,'INDEX=',I3,3X,'MULT=',I3)


END SUBROUTINE wien_structfile

SUBROUTINE wien_energyfiles(nat)
  USE reallocate
  USE defs
  USE input
  USE bandstructure
  IMPLICIT NONE
  INTEGER                    :: nat
  REAL(8),ALLOCATABLE        :: bandenergy_temp(:,:),bandenergyup(:)
  INTEGER,ALLOCATABLE        :: ne(:)
  CHARACTER(10)              :: kname
  REAL(8)                    :: emist,s,t,z,e1,num
  INTEGER                    :: neup,nume
  INTEGER                    :: i,k,n,ios,ii
!========================================================
!=========         READ energy file      ================
!========================================================
!find nkpt and nband 
  k=0  
  nume=0
  DO I=1,NAT 
     READ(10,'(f9.5)') EMIST
     READ(10,'(f9.5)') EMIST
  ENDDO
  DO
     READ(10,'(3e19.12,a10,2i6)',IOSTAT=ios) S,T,Z,KNAME,N,NEup
     IF (ios /= 0) EXIT
     k=k+1
     nume=MAX(neup,nume)
     DO ii=1,neup
        READ(10,*) NUM,E1
     ENDDO
  ENDDO
  nkpt=k
  WRITE(6,*) 'Number of kpts in IBZ: ',nkpt
  REWIND(10)

  CALL init_bandstructure(nume,nkpt+1)
  ALLOCATE(ne(nkpt))
  allocate (bandenergy_temp(nume,nkpt+1))
  allocate (bandenergyup(nume))
  k=0
  DO I=1,NAT 
     READ(10,'(f9.5)') EMIST
     READ(10,'(f9.5)') EMIST
  ENDDO
  DO 
     READ(10,'(3e19.12,a10,2i6)',IOSTAT=ios) xkpoint(1:3,k+1),KNAME,N,NEup!,weight(k+1)
     IF (ios /= 0) EXIT
     k=k+1
     DO ii=1,neup
        READ(10,*) NUM,bandenergyup(ii)
     ENDDO
     ne(k)=neup
     DO ii=1,ne(k)
        bandenergy_temp(ii,k)=bandenergyup(ii)
     ENDDO
  ENDDO
  nband=MINVAL(ne(1:nkpt))
  WRITE(6,*) 'Number of bands: ',nband

  ! convert k-points to primitive lattice 
  IF(lattice=='F'.OR.lattice=='I'.OR.lattice=='A'.OR.lattice=='B'.OR.lattice=='C') THEN
     DO i=1,nkpt
        xkpoint(1:3,i)=MATMUL(TRANSPOSE(p2c_dir),xkpoint(1:3,i))
     ENDDO
     WRITE(6,*) 'k-points converted to primitive'
  ENDIF
!!$  ELSEIF(lattice=='R') THEN
!!$     WRITE(6,*) 'R lattice. k-points converted to BoltzTrap basis.'
!!$     ! Peters basis. Taken from latgen.f in SRC_lapw1
!!$     temp(1,1) =  1.0D+0/(SQRT(3.0D+0)*aa(1)); temp(1,2) =  1.0D+0/(SQRT(3.0D+0)*aa(1)); temp(1,3) = -2.0D+0/(SQRT(3.0D+0)*aa(1))
!!$     temp(2,1) = -1.0D+0/aa(2); temp(2,2) =  1.0D+0/aa(2); temp(2,3) =  0.0D+0
!!$     temp(3,1) =  1.0D+0/aa(3); temp(3,2) =  1.0D+0/aa(3); temp(3,3) =  1.0D+0/aa(3)
  bandenergy(1:nband,1:nkpt)=bandenergy_temp(1:nband,1:nkpt)

  deALLOCATE(ne)
  deallocate (bandenergy_temp)
  deallocate (bandenergyup)
END SUBROUTINE wien_energyfiles
