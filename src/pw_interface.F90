subroutine pw_interface(idebug)
! interface for  pwsic band structure style. Pietro delugas sep 2010
      use bandstructure
      implicit none
      integer isym,idebug,ik
      real(8)  wk
      integer nume
      real(8),allocatable ::  bandenergy_temp(:,:)
      real(8), parameter :: PI=3.1415926536d0
      lattice='P'

      open (unit=69,file='pw_struct',status='old')
      read (69,*) aa(1),aa(2),aa(3),alpha(1),alpha(2),alpha(3)
      alpha=alpha*PI/180d0
      read (69,*)  nsym
      read(69,*)  symop(:,:,1:nsym)
      do isym =1,nsym
        write (6,'(3f8.2)') symop(:,:,isym)
        write (6,*)  isym
      end do
      read(69,*) tau(:,nsym)
      call latgen2('xayb',lattice,aa,alpha,volume,aac_dir,p2c_dir,aac_rec,p2c_rec)
      close (69)
      open (unit=91,file='bands4boltrap',status='old')

      read (91,*) nkpt,nband
      write (6,*) nkpt,nband
      CALL init_bandstructure(nband,nkpt)
      allocate (bandenergy_temp(nband, nkpt))
      bandenergy_temp = 0.0d0
      nume = 0
      do ik=1,nkpt
         read (91,*) xkpoint(:,ik),wk,nume
!          read (91,*) xkpoint(:,ik),dummy,nume
!          write (6,*) ik,xkpoint(:,ik),dummy,nume,wk
         if (nume.lt.nband) nband=nume

         read (91,*) bandenergy_temp(1:nume,ik)
      end do
      ! reset contents of bandstructure module
      deallocate (bandenergy)
      allocate(bandenergy(nband,nkpt))
      bandenergy = 0.0d0
      bandenergy(1:nband,:)=bandenergy_temp(1:nband,:)
      deallocate (bandenergy_temp)
      end subroutine

