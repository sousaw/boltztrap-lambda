#!/bin/csh -f
unalias rm

set running = ".running.$$.`hostname`.`date +%d%m%H%M%S`"
echo $$ >$running
onintr clear
alias error	'echo ">>> ($name) \!* -> exit"; goto error'

set name	= $0		#full name of script-file
set bin		= $name:h	#default directory for WIEN-executables
if !(-d $bin) set bin = .
set name	= $name:t

set def 	= def		#def-file
set tmp		= ( )		#tmp-files, to be deleted before exit
set log		= :log		#log-file
set t		= time		#
set updn			# spinpolarization switch
set dnup	= 'dn'		# spinpolarization switch
set sc				# semicore-switch
set so				# spinorbit-switch
set sodum			# spinorbit-switch
set cmplx
set para                        # parallel execution 
set dopara                        # parallel execution 
set sel                         # standard vector file in lapw7
set band1                       # regular run in lapw1
unset orb                      # LDA+U in lapw1
unset iter                      # iterative diagonalization
unset nohns                     # without HNS in lapw1
unset qtl                       # regular run in lapw2
unset band                       # regular run in lapw2
unset fermi                     # regular run in lapw2
unset efg                       # regular run in lapw2
unset command			#command
unset file			#file-head
unset deffile_only		#create just def file
set scratch =                   #eg: /scr1/pblaha/   for vectors, help-files,

if ( $?SCRATCH ) then
  set scratch=`echo $SCRATCH  | sed -e 's/\/$//'`/ # we are afraid
				# different settings in different 
				# computing centers
			        #use global variable for scratch if set
endif

set argv1=($argv)

while ($#argv)
  switch ($1)
  case -f:
    shift; set file = $1
    shift; breaksw
  case -h:
  case -H: 
    set help
    shift; breaksw
  case -[t|T]:
    set t
    shift; breaksw
  case -up:
    set updn = 'up'
    set dnup = 'dn'
    shift; breaksw
  case -dn:
    set updn = 'dn'
    set dnup = 'up'
    shift; breaksw
  case -du:
    set du = 'du'
    set updn = 'up'
    shift; breaksw
  case -sc:
    set sc = 's'
    shift; breaksw
  case -d:
    set deffile_only
    shift; breaksw
  case -c:
    set cmplx = c
    shift; breaksw
  case -[p|P]
    set para = para
    shift; breaksw
  case -it:
    set iter
    shift; breaksw	
  case -orb:
    set orb
    shift; breaksw	
  case -nohns:
    set nohns
    shift; breaksw	
  case -so:
    set so = 'so'
    set sodum = 'dum'
    set cmplx = c
    shift; breaksw
  case -band:
    set band
    set band1='_band'
    shift; breaksw
  case -qtl:
    set qtl
    shift; breaksw
  case -efg:
    set efg
    shift; breaksw
  case -sel:
    set sel = f
    shift; breaksw
  case -fermi:
    set fermi
    shift; breaksw  
  default:
    set command = $1
    shift; breaksw
  endsw
end

if !($?file) then 
set file    = `pwd`
set file    = $file:t		#tail of file-names
endif


if ( $?help ) goto help

if !($?command) then
  error no command
endif

if ($command == lapw1c) then
  set cmplx = c
  set command = lapw1
else if ($command == lapw1it) then
  set iter
  set command = lapw1
else if ($command == lapw1itc) then
  set cmplx = c
  set iter
  set command = lapw1
else if ($command == lapw2c) then
  set cmplx = c
  set command = lapw2
else if ($command == lapwdmc) then
  set cmplx = c
  set command = lapwdm
else if ($command == dmatc) then
  set cmplx = c
  set command = dmat
else if ($command == filtvecc) then
  set cmplx = c
  set command = filtvec
else if ($command == lapw7c) then
  set cmplx = c
  set command = lapw7
else if ($command == lapw5c) then
  set cmplx = c
  set command = lapw5
else if ($command == spinorbitc) then
  set cmplx = c
  set command = spinorbit
else if ($command == opticc) then
  set cmplx = c
  set command = optic
endif

echo "`date`> ($name) $command $argv1[2-]" >> $log

set def	= $updn$command$sc.def
#touch $def

switch ($command)

#30,'$file.klist',         'old',    'formatted',0
#11,'$file.energy$so$dnup',         'unknown',    'formatted',0
case BoltzTraP:
set exe = BoltzTraP
cat << theend > $def
5, '$file.intrans',      'old',    'formatted',0
6,'$file.outputtrans',      'unknown',    'formatted',0
20,'$file.struct',         'old',    'formatted',0
10,'$file.energy$so$updn',         'old',    'formatted',0
48,'$file.engre',         'unknown',    'unformatted',0
49,'$file.transdos',        'unknown',    'formatted',0
50,'$file.sigxx',        'unknown',    'formatted',0
51,'$file.sigxxx',        'unknown',    'formatted',0
52,'$file.sigxx_l',        'unknown',    'formatted',0
21,'$file.trace',           'unknown',    'formatted',0
22,'$file.condtens',           'unknown',    'formatted',0
24,'$file.halltens',           'unknown',    'formatted',0
30,'${file}_BZ.cube',           'unknown',    'formatted',0
35,'${file}.banddat',           'unknown',    'formatted',0
36,'${file}_band.gpl',           'unknown',    'formatted',0
theend
breaksw

#23,'$file.seebtens',           'unknown',    'formatted',0
#25,'$file.thermaltens',           'unknown',    'formatted',0
#37,'${file}_deriv.dat',           'unknown',    'formatted',0
#38,'${file}_mass.dat',           'unknown',    'formatted',0

default:
error	command $command does not exist
breaksw

endsw

if ($?deffile_only) then
  rm $running
  exit(0)
endif

if ( $para == para && $dopara) then
   if ( $updn == up ) then
      set exe = ($exe -up)
   else if ($updn == dn ) then
      set exe = ($exe -dn)
   endif
   if ($cmplx == c ) then
      set exe = ($exe -c)
   endif
   if ($so == so) then
      set exe = ($exe -so)
   endif
endif  	

echo $exe >>$running
$t $bin/$exe $def 
# rm $def $tmp
clear:
#cleanup
if ($?qtl || $?band || $?fermi || $?efg) then
  if( -e .oldin2 ) mv .oldin2 $file.in2$cmplx
#  if( -e .oldin1 ) mv .oldin1 $file.in1$cmplx
endif
if (-f $running) rm $running
exit(0)

error:
rm $running

help:					#help exit 
cat << theend 

USAGE:	$0 PROGRAMNAME [flags] 

PURPOSE:runs WIEN executables:                         afminput,aim,clmcopy,
	lcore,dmat,dstart,eosfit,init_xspec,hex2rhomb,irrep,joint,kgen,kram,
        lapw0,lapw1,lapw2,lapw3,lapw5,lapwdm,lapwso,orb,lorentz,lstart,mini,
        mixer,nn,optic,optimize,rhomb_in5,sgroup,spaghetti,sumpara,supercell,
	symmetry,symmetso,tetra,txspec,xspec,elnes,telnes,filtvec,lapw7,qtl

FLAGS:
-f FILEHEAD ->	FILEHEAD for path of struct & input-files
-t/-T ->	suppress output of running time
-h/-H ->	help
-d    ->	create only the def-file 
-up   ->	runs up-spin
-dn   ->	runs dn-spin
-du   ->	runs up/dn-crossterm
-sc   ->	runs semicore calculation
-c    ->	complex calculation (no inversion symmetry present)	
-p    ->        run lapw0/1/2/so/dm/optic in parallel (needs .machines file)
-orb  ->	runs lapw1 with LDA+U/OP or B-ext correction
-it   ->	runs lapw1 with iterative diagonalization
-nohns->	runs lapw1 without HNS
-qtl  ->        calculates QTL in lapw2
-band ->        for bandstructures: uses *klist_band, sets QTL and ROOT (in2)
-fermi->        calculates lapw2 with FERMI switch
-efg  ->        calculates lapw2 with EFG switch
-so   ->	runs lapw2/optic/spaghetti with def-file for spin-orbit calc.
-sel  ->        use reduced vector file in lapw7
theend
if !($?command) then
  set command
endif

switch ($command)

case afminput:
echo 'x afminput'
breaksw
case aim:
echo 'x aim [-c]'
breaksw
case clmcopy:
echo 'x clmcopy'
breaksw
case dstart:
echo 'x dstart [-up/-dn -c]'
breaksw
case eosfit: 
echo 'x eosfit'
breaksw
case init_xspec: 
echo 'x init_xspec [-up/-dn]'
breaksw
case irrep: 
echo 'x irrep [-so -up/-dn -p]'
breaksw
case joint: 
echo 'x joint [-up/-dn]'
breaksw
case kgen: 
echo 'x kgen [-so]     (-so switch only when you have a case.ksym file generated by initso_lapw)'
breaksw
case kram: 
echo 'x kram [-up/-dn]'
breaksw
case lapw0: 
echo 'x lapw0 [-p]'
breaksw
case lapw1: 
echo 'x lapw1 [-c -up/-dn -it -p -nohns -orb -band]'
breaksw
case lapw2: 
echo 'x lapw2 [-c -up/-dn -p -so -qtl -fermi -efg -band]'
breaksw
case lapw3: 
echo 'x lapw3 [-c]'
breaksw
case lapw5: 
echo 'x lapw5 [-c -up/-dn]'
breaksw
case filtvec: 
echo 'x filtvec [-c -up/-dn]'
breaksw
case lapw7: 
echo 'x lapw7 [-c -up/-dn -sel]'
breaksw
case lapwdm: 
echo 'x lapwdm [ -up -p -c]'
breaksw
case lapwso: 
echo 'x lapwso [ -up -p -c -orb]'
breaksw
case lcore: 
echo 'x lcore [-up/-dn]'
breaksw
case lorentz: 
echo 'x lorentz [-up/-dn]'
breaksw
case lstart: 
echo 'x lstart'
breaksw
case mini: 
echo 'x mini'
breaksw
case mixer: 
echo 'x mixer'
breaksw
case nn: 
echo 'x nn'
breaksw
case optic: 
echo 'x optic [-c -up/-dn -so -p]'
breaksw
case optimize: 
echo 'x optimize'
breaksw
case orb: 
echo 'x orb [ -up/-dn/-du ]'
breaksw
case qtl: 
echo 'x qtl [ -up/-dn -so -p]'
breaksw
case sgroup: 
echo 'x sgroup'
breaksw
case spaghetti: 
echo 'x spaghetti [-up/-dn -so -p]'
breaksw
case sumpara: 
echo 'x sumpara -d [-up/-dn/-du]     and than '
echo 'sumpara [up/dn]sumpara.def #_of_processors'
breaksw
case supercell: 
echo 'x supercell'
breaksw
case symmetry: 
echo 'x symmetry'
breaksw
case symmetso: 
echo 'x symmetso [-c]'
breaksw
case tetra: 
echo 'x tetra [-up/-dn]'
breaksw
case txspec: 
echo 'x txspec [-up/-dn]'
breaksw
case xspec: 
echo 'x xspec [-up/-dn]'
breaksw
case elnes:
echo 'x elnes [-up/-dn]'
breaksw
case telnes:
echo 'x telnes [-up/-dn]'
breaksw
case initelnes:
echo 'x initelnes [-up/-dn]'
breaksw

default:
echo 'USE: x -h PROGRAMNAME   for valid flags for a specific program'
breaksw

endsw
if (-e $running) rm $running

exit(1)
