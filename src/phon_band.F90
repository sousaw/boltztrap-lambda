SUBROUTINE phon_band
  USE input
  USE constants
  USE bandstructure
  IMPLICIT NONE
  REAL(8)                  :: peso,converter
  INTEGER                  :: nat,iq,k,ivec
  COMPLEX(8),ALLOCATABLE   :: eigenmatrix(:)

  WRITE(6,'(A)') '==============  OUTPUT PHON-WIEN interface================'
  CALL wien_structfile(nat)

  READ(10,*) nkpt,nband
  ALLOCATE(eigenmatrix(nband))
  CALL init_bandstructure(nband,nkpt)
  converter=1E12*HBAR_SI*TWO*PI*JOULE/RYDBERG
  DO iq=1,nkpt
     READ(10,*) xkpoint(1:3,iq)
     READ(10,*) peso
     DO ivec = 1, nband
        READ(10,*) bandenergy(ivec,iq)
        IF(bandenergy(ivec,iq)>ZERO) THEN
           bandenergy(ivec,iq) = SQRT(bandenergy(ivec,iq))*converter
        ELSE
           bandenergy(ivec,iq) = -SQRT(-bandenergy(ivec,iq))*converter
        ENDIF
        DO k=1,nband
           READ(10,*) eigenmatrix(k)
        ENDDO
     ENDDO
  ENDDO
  nval=nband/TWO
  eferm=MAXVAL(bandenergy)/TWO
  ecut=MAXVAL(bandenergy)
  deltae=ecut/500.
  WRITE(6,'(A)') '==============  End PHON-WIEN interface    ==============='
END SUBROUTINE phon_band

