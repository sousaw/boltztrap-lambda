LOGICAL FUNCTION kdelta(k,nst,stg)
  !.... TEST, IF K IS IN STAR OF G  (GENERATED IN STERN)                  
  USE defs
  IMPLICIT NONE
  INTEGER          :: nst
  REAL(8)          :: k(3),stg(3,nst)
  INTEGER          :: i
  !---------------------------------------------------------------------  
  KDELTA=.FALSE.
  DO I=1,NST
     IF((ABS(stg(1,i)-k(1))+ABS(stg(2,i)-k(2))+ABS(stg(3,i)-k(3))).LT.TEST) kdelta=.TRUE.
  ENDDO
END FUNCTION kdelta
