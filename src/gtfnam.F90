      SUBROUTINE GTFNAM(DEFFN,ERRFN,IPROC)
      IMPLICIT NONE
      CHARACTER*(*)     ::  DEFFN, ERRFN
      INTEGER           :: iproc
!        Local Parameters
!
      CHARACTER(5)        ERREXT
      PARAMETER          (ERREXT = 'error')
!
!        Local Scalars
!
      INTEGER            I
!
!        extract the command-line argument
!
      CALL GETARG(0,DEFFN)
      IF (DEFFN .EQ. '      ') THEN
         CALL GETARG(3,DEFFN)
         IF (DEFFN .NE. '      ') then
            READ(DEFFN,*)IPROC
         else
            IPROC=0
         endif
         CALL GETARG(2,DEFFN)
         IF (DEFFN .EQ. '      ') GOTO 900
      ELSE
         deffn=''
         CALL GETARG(2,DEFFN)
         IF (DEFFN .NE. '      ') then
            READ(DEFFN,*)IPROC
         else
            IPROC=0
         endif
         CALL GETARG(1,DEFFN)
         IF (DEFFN .EQ. '      ') GOTO 900
      ENDIF
!
!        generate a name for the error-message file
!
      DO 10 I = LEN(DEFFN), 1, -1
         IF (DEFFN(I:I) .EQ. '.') THEN
            IF (LEN(ERRFN) .LT. (I+LEN(ERREXT))) GOTO 910
            ERRFN(1:I) = DEFFN(1:I)
            ERRFN(I+1:LEN(ERRFN)) = ERREXT
            GOTO 30
         ENDIF
   10 CONTINUE
!
!        the name of the definition file contains no '.', it is assumed
!        that this name contains no extension - append the extension
!        '.error' to get a name for the error file.
!
      DO 20 I = LEN(DEFFN), 1, -1
         IF (DEFFN(I:I) .NE. ' ') THEN
            IF (LEN(ERRFN) .LT. (I+1+LEN(ERREXT))) GOTO 910
            ERRFN(1:I) = DEFFN(1:I)
            ERRFN(I+1:LEN(ERRFN)) = '.' // ERREXT
            GOTO 30
         ENDIF
   20 CONTINUE
!
!        filename contains only spaces
!
      GOTO 900
   30 CONTINUE
!      write(*,*) 'DEFFN = ', DEFFN
!      write(*,*) 'ERRFN = ', ERRFN
!
      RETURN
!
!        Errors
!
  900 STOP 'GTFNAM - Exactly one commandline argument has to be given.'
  910 STOP 'GTFNAM - string ERRFN too short to hold filename.'
!
!        End of 'GTFNAM'
!
      END

